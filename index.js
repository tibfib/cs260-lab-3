var app = angular.module('App', []);

app.controller('LengthCtrl', [
    '$scope',
    function($scope) {
        $scope.updateMeters = function(newValue) {
            if (newValue !== undefined) {
                $scope.feet = 3.2808399 * newValue;
                $scope.inches = 39.37007874 * newValue;
                $scope.yards = 1.093613298 * newValue;
                $scope.miles = 0.0006213711922 * newValue;
                $scope.centimeters = 100 * newValue;
            }
        };

        $scope.updateFeet = function(newValue) {
            if (newValue !== undefined) {
                $scope.meters = 0.3048 * newValue;
                $scope.inches = 12 * newValue;
                $scope.yards = 0.3333333333 * newValue;
                $scope.miles = 0.0001893939394 * newValue;
                $scope.centimeters = 30.48 * newValue;
            }
        };

        $scope.updateCentimeters = function(newValue) {
            if (newValue !== undefined) {
                $scope.feet = 0.03280839895 * newValue;
                $scope.meters = 0.01 * newValue;
                $scope.inches = 0.3937007874 * newValue;
                $scope.yards = 0.01093613298 * newValue;
                $scope.miles = 0.000006213711922 * newValue;
            }
        };

        $scope.updateYards = function(newValue) {
            if (newValue !== undefined) {
                $scope.centimeters = 91.44 * newValue;
                $scope.feet = 3 * newValue;
                $scope.meters = 0.9144 * newValue;
                $scope.inches = 36 * newValue;
                $scope.miles = 0.0005681818182 * newValue;
            }
        };
        $scope.updateMiles = function(newValue) {
            if (newValue !== undefined) {
                $scope.centimeters = 160934.4 * newValue;
                $scope.feet = 5280 * newValue;
                $scope.meters = 1609.344 * newValue;
                $scope.inches = 63360 * newValue;
                $scope.yards = 1760 * newValue;
            }
        };

        $scope.updateInches = function(newValue) {
            if (newValue !== undefined) {
                $scope.centimeters = 2.54 * newValue;
                $scope.feet = 0.08333333333 * newValue;
                $scope.meters = 0.0254 * newValue;
                $scope.miles = 0.00001578282828 * newValue;
                $scope.yards = 0.02777777778 * newValue;
            }
        };
    }
]);

app.directive('row', function() {
    return {
        scope: {
            value: '=',
            label: '@',
            onChange: '='
        },
        restrict: 'E',
        template:
            '<div class="row">' +
            '<label for="{{id}}}">{{label}}</label>' +
            '<input type="number" name="{{id}}" ng-model="value" ng-change="onChange(value)" />' +
            '</div>',
        link: function(scope) {
            console.log('scope.onChange', scope.onChange);
            scope.id = Math.random() * 2000 + '_val';
        }
    };
});
